// Copyright (C) 2020 Johan M. Cos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


import React from 'react';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  buttonPadding: {
    // padding: '20px',
    marginRight: '20px'
  }
})

function App() {
  const classes = useStyles()
  return (
    <div className="App">
      <CssBaseline />
      <header className="App-header">
        stuff
      </header>
      <Container>
        <Typography variant={'h2'} gutterBottom>Step One</Typography>
        <div>
        <Button variant={'contained'} color={'primary'} className={classes.buttonPadding}>
          Submit
        </Button>
        <TextField required={true} placeholder={"Enter your letters here"} />
        </div>

      </Container>
    </div>
  );
}

export default App;
