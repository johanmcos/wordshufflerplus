# WordShufflerPlus

Create a simple web application that allows a user to provide a set of letters, fix certain ones in place, and then shuffle the remaining ones

## Why?

The simplest reason is to get some practice creating a simple SPA for my personal development.

However, I'm also doing it because I feel it would be useful. I play games like 'Wordscapes' that involve guessing words from a set of letters. Most games include a shuffle utility, but it shuffles all letters, which is annoying if you already know what the word starts or ends with.

I've found tools that let you search words with a certain set of letters, but I feel that because it's using a dictionary, it's cheating.

This project does not rely on a dictionary, only on the ability to assign letters to random places. The 'words' it suggests are mostly gibberish and it's up to the user to distinguish which ones are actual words in the English language.

Is that still cheating? Well, I won't tell if you don't ;-)

## License

Copyright (C) 2020 Johan M. Cos

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
